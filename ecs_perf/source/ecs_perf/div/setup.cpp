#include "ecs_perf/div/setup.h"

namespace ecs_perf::div
{
MovementSystem::MovementSystem(::diversity::framework::EntityManager &entityManager)
    : m_entityManager(entityManager), m_componentIndex(entityManager.componentIndex()), m_tracker(entityManager)
{
}

void MovementSystem::update()
{
   for (auto eId : m_tracker.tracks())
   {
      auto pPos = m_componentIndex.getComponent<Pos>(eId);
      auto pVel = m_componentIndex.getComponent<Vel>(eId);
      pPos->m_p += pVel->m_v;
   }
}
} // namespace ds1