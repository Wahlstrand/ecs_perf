#pragma once

#include "diversity/framework/entity_manager.h"
#include "diversity/framework/system_manager.h"
#include "diversity/framework/entity_tracker.h"
#include "diversity/framework/icomponent.h"
#include "ecs_perf/common/setup.h"

namespace ecs_perf::div
{
enum ComponentType
{
   pos,
   vel,
};
struct Pos : public ::diversity::framework::IComponent
{
   static ::diversity::framework::ComponentTypeId typeId() { return ComponentType::pos; }
   Pos(double x, double y) : m_p{.x = x, .y = y} {}
   Pos() : m_p() {}
   common::Vec2 m_p;
};
struct Vel : public ::diversity::framework::IComponent
{
   static ::diversity::framework::ComponentTypeId typeId() { return ComponentType::vel; }
   common::Vec2 m_v;
};

class MovementSystem : public ::diversity::framework::ISystem
{
public:
   MovementSystem(::diversity::framework::EntityManager &entityManager);
   void update() final;

private:
   ::diversity::framework::EntityManager &m_entityManager;
   ::diversity::framework::ComponentIndex &m_componentIndex;
   ::diversity::framework::EntityTracker<Pos, Vel> m_tracker;
};
} // namespace ecs_test::div
