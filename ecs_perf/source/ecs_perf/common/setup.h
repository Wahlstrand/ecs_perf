#pragma once

namespace ecs_perf::common
{
struct Vec2
{
   double x = 0.0;
   double y = 0.0;
};
inline Vec2 &operator+=(Vec2 &lhs, const Vec2 &rhs)
{
   lhs.x += rhs.x;
   lhs.y += rhs.y;
   return lhs;
}
} // namespace ecs_perf::common
