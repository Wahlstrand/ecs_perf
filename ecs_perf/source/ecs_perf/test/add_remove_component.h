#pragma once
#include "ecs_perf/ecs/setup.h"
#include "ecs_perf/div/setup.h"

#include <benchmark/benchmark.h>

namespace ecs_perf
{
namespace ecs
{
void addRemoveComponents(ecs_perf::ecs::ECST &ecs, size_t nEntities)
{
   for (size_t n = 0; n < nEntities; n++)
   {
      ecs.addComponents<Pos>(n, {});
   }
   for (size_t n = 0; n < nEntities; n++)
   {
      ecs.removeComponents<Pos>(n);
   }
}
void testAddRemoveComponentsN(size_t nEntities, benchmark::State &state)
{
   //Setup
   using ecs_perf::ecs::ECST;
   ECST ecs;
   for (size_t n = 0; n < nEntities; n++)
   {
      ecs.addEntity();
   }

   //Run once to avoid the caching process
   addRemoveComponents(ecs, nEntities);

   //Test
   for (auto _ : state)
   {
      addRemoveComponents(ecs, nEntities);
   }
}
} // namespace ecs

namespace div
{
void addRemoveComponentsN(std::vector<diversity::framework::Entity> &entities, size_t nEntities)
{
   for (size_t n = 0; n < nEntities; n++)
   {
      entities[n].addComponent<Pos>();
   }
   for (size_t n = 0; n < nEntities; n++)
   {
      entities[n].removeComponent<Pos>();
   }
}

void testAddRemoveComponentsN(size_t nEntities, benchmark::State &state)
{
   //Setup
   diversity::framework::EntityManager em;
   std::vector<diversity::framework::Entity> entities;
   for (size_t n = 0; n < nEntities; n++)
   {
      entities.push_back(em.addEntity());
   }

   //Run once to avoid the caching process
   addRemoveComponentsN(entities, nEntities);

   //Test
   for (auto _ : state)
   {
      addRemoveComponentsN(entities, nEntities);
   }
}
} // namespace div
} // namespace ecs_perf
static void BM_ecs_add_remove_component_10000(benchmark::State &state)
{
   ecs_perf::ecs::testAddRemoveComponentsN(10000, state);
}
static void BM_div_add_remove_component_10000(benchmark::State &state)
{
   ecs_perf::div::testAddRemoveComponentsN(10000, state);
}