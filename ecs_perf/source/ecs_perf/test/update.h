#pragma once
#include "ecs_perf/ecs/setup.h"
#include "ecs_perf/div/setup.h"

#include <benchmark/benchmark.h>

namespace ecs_perf
{
namespace ecs
{
void updateComponents(size_t nEntities, benchmark::State &state)
{
   //Setup
   using ecs_perf::ecs::ECST;
   using ecs_perf::ecs::MovementSystem;
   using ecs_perf::ecs::Pos;
   using ecs_perf::ecs::Vel;
   ECST ecs;
   MovementSystem sys(ecs);

   for (size_t n = 0; n < nEntities; n++)
   {
      auto e = ecs.addEntity();
      ecs.addComponents<Pos, Vel>(e, {}, {});
   }

   //Test
   for (auto _ : state)
   {
      sys.update();
   }
}
} // namespace ecs
namespace div
{
void updateComponents(size_t nEntities, benchmark::State &state)
{
   //Setup
   using ecs_perf::div::MovementSystem;
   using ecs_perf::div::Pos;
   using ecs_perf::div::Vel;
   diversity::framework::EntityManager em;
   diversity::framework::SystemManager sm;
   MovementSystem sys(em);

   for (size_t n = 0; n < nEntities; n++)
   {
      auto e1 = em.addEntity();
      e1.addComponent<Pos>();
      e1.addComponent<Vel>();
   }

   //Test
   for (auto _ : state)
   {
      sys.update();
   }
}
} // namespace div
} // namespace ecs_perf

static void BM_ecs_update_components_100(benchmark::State &state)
{
   ecs_perf::ecs::updateComponents(100, state);
}
static void BM_ecs_update_components_10000(benchmark::State &state)
{
   ecs_perf::ecs::updateComponents(10'000, state);
}
static void BM_div_update_components_100(benchmark::State &state)
{
   ecs_perf::div::updateComponents(100, state);
}
static void BM_div_update_components_10000(benchmark::State &state)
{
   ecs_perf::div::updateComponents(10'000, state);
}
