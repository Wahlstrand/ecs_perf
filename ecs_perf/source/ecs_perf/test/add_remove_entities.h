#pragma once
#include "ecs_perf/ecs/setup.h"
#include "ecs_perf/div/setup.h"

#include <benchmark/benchmark.h>

namespace ecs_perf
{
namespace ecs
{
void addRemoveEntitiesNM(ecs_perf::ecs::ECST &ecs, size_t nEntities, size_t nTimes)
{
   for (size_t n = 0; n < nTimes; n++)
   {
      for (size_t id = 0; id < nEntities; id++)
      {
         ecs.addEntity();
      }
      for (size_t id = 0; id < nEntities; id++)
      {
         ecs.removeEntity(id);
      }
   }
}
void testAddRemoveEntitiesNM(size_t nEntities, size_t nTimes, benchmark::State &state)
{
   //Setup
   using ecs_perf::ecs::ECST;
   ECST ecs;

   //Run once to avoid the caching process
   addRemoveEntitiesNM(ecs, nEntities, nTimes);

   //Test
   for (auto _ : state)
   {
      addRemoveEntitiesNM(ecs, nEntities, nTimes);
   }
}
} // namespace ecs

namespace div
{
void addRemoveEntitiesNM(diversity::framework::EntityManager &em, size_t nEntities, size_t nTimes)
{
   for (size_t n = 0; n < nTimes; n++)
   {
      for (size_t id = 0; id < nEntities; id++)
      {
         em.addEntity();
      }
      for (size_t id = 0; id < nEntities; id++)
      {
         em.removeEntity(id);
      }
   }
}

void testAddRemoveEntitiesNM(size_t nEntities, size_t nTimes, benchmark::State &state)
{
   //Setup
   diversity::framework::EntityManager em;

   //Run once to avoid the caching process
   addRemoveEntitiesNM(em, nEntities, nTimes);

   //Test
   for (auto _ : state)
   {
      addRemoveEntitiesNM(em, nEntities, nTimes);
   }
}
} // namespace div
} // namespace ecs_perf

static void BM_ecs_add_remove_1_entities_1_times(benchmark::State &state)
{
   ecs_perf::ecs::testAddRemoveEntitiesNM(1, 1, state);
}
static void BM_div_add_remove_1_entities_1_times(benchmark::State &state)
{
   ecs_perf::div::testAddRemoveEntitiesNM(1, 1, state);
}
static void BM_ecs_add_remove_1000_entities_1_times(benchmark::State &state)
{
   ecs_perf::ecs::testAddRemoveEntitiesNM(1000, 1, state);
}
static void BM_div_add_remove_1000_entities_1_times(benchmark::State &state)
{
   ecs_perf::div::testAddRemoveEntitiesNM(1000, 1, state);
}
static void BM_ecs_add_remove_1_entities_100_times(benchmark::State &state)
{
   ecs_perf::ecs::testAddRemoveEntitiesNM(1, 1000, state);
}
static void BM_div_add_remove_1_entities_100_times(benchmark::State &state)
{
   ecs_perf::div::testAddRemoveEntitiesNM(1, 1000, state);
}
static void BM_ecs_add_remove_1000_entities_100_times(benchmark::State &state)
{
   ecs_perf::ecs::testAddRemoveEntitiesNM(1000, 100, state);
}
static void BM_div_add_remove_1000_entities_100_times(benchmark::State &state)
{
   ecs_perf::div::testAddRemoveEntitiesNM(1000, 100, state);
}
