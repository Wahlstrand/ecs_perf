#pragma once

#include "ecs/ecs.h"
#include "ecs/isystem.h"
#include "ecs/entity_tracker.h"
#include "ecs_perf/common/setup.h"

namespace ecs_perf::ecs
{
struct Pos
{
   common::Vec2 m_p;
};
struct Vel
{
   common::Vec2 m_v;
};

typedef ::ecs::ECS<Pos, Vel> ECST;

class MovementSystem final : public ::ecs::ISystem
{
public:
   MovementSystem(ECST &ecs);
   void update() final;

private:
   ECST &m_ecs;
   ::ecs::EntityTracker<ECST, Pos, Vel> m_tracker;
};
} // namespace ecs_perf::ecs
