#include "ecs_perf/ecs/setup.h"

namespace ecs_perf::ecs
{
MovementSystem::MovementSystem(ECST &ecs)
    : m_ecs(ecs), m_tracker(ecs)
{
}
void MovementSystem::update()
{
   for (auto eId : m_tracker.tracks())
   {
      auto &pos = m_ecs.getComponent<Pos>(eId);
      const auto &vel = m_ecs.getComponent<Vel>(eId);

      pos.m_p += vel.m_v;
   }
}
} // namespace ecs_perf::ecs
