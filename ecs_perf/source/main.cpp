#include <benchmark/benchmark.h>

#include "ecs_perf/test/add_remove_entities.h"
#include "ecs_perf/test/add_remove_component.h"
#include "ecs_perf/test/update.h"

BENCHMARK(BM_ecs_update_components_100);
BENCHMARK(BM_div_update_components_100);
BENCHMARK(BM_ecs_update_components_10000);
BENCHMARK(BM_div_update_components_10000);

BENCHMARK(BM_ecs_add_remove_1_entities_1_times);
BENCHMARK(BM_div_add_remove_1_entities_1_times);
BENCHMARK(BM_ecs_add_remove_1000_entities_1_times);
BENCHMARK(BM_div_add_remove_1000_entities_1_times);
BENCHMARK(BM_ecs_add_remove_1_entities_100_times);
BENCHMARK(BM_div_add_remove_1_entities_100_times);
BENCHMARK(BM_ecs_add_remove_1000_entities_100_times);
BENCHMARK(BM_div_add_remove_1000_entities_100_times);

BENCHMARK(BM_ecs_add_remove_component_10000);
BENCHMARK(BM_div_add_remove_component_10000);

BENCHMARK_MAIN();